#! bin/bash
#Script builds and runs new experiment in named folder 
#flags:
#-n the name of the experiment. will also be the name of root

while getopts 'n:' flag; do
  case $flag in
    n) 
	echo "flag $flag is $OPTARG"
	name=$OPTARG
	echo $name
	;;
   # v) verbose='true' ;;
    *) print_usage
       exit 1 ;;
  esac
done

mkdir "experiments/$name"
cd "experiments/$name"
echo "preparing folders for training"
mkdir -p "trained_models" "logs"
echo > "logs/${name}.log"
echo "beginning training ...."
pwd
python3 ../../src/train_baseline_snli.py --train_file "../preprocessed_data/conn-train.hdf5" --dev_file "../preprocessed_data/conn-val.hdf5" --test_file "../preprocessed_data/conn-test.hdf5" --w2v_file "../preprocessed_data/glove.hdf5" --log_dir "logs/" --log_fname "${name}.log" --gpu_id -1 --model_path "trained_models/${name}-conn-insertion" --epoch 50
done

